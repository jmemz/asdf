import { StatusBar } from 'expo-status-bar';
import React, {useState} from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { TextInput, Dimensions, Button, Alert } from 'react-native';

export default function App() {
  const [word,SetWord] = React.useState ("");
  const pressHandler = ()=>{
    alert ('You just typed: ' + word);
  }
  return (
    <View style={styles.container}>
      <Text>ENTER ANY TEXT</Text>
      <StatusBar style="auto" />
      <TextInput 
      style={styles.input}
      placeholder='e.g. name'
      onChangeText={(text) => SetWord (text)}
      value = {word}
      />
      <Button
        title = 'SUBMIT'
        onPress={pressHandler}
        color='#0081a7'

      />
    </View>
  );
}
const {width, height} = Dimensions.get("screen");
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fdfcdc',
    alignItems: 'center',
    justifyContent: 'center',
  },
  input: {
    backgroundColor: '#fed9b7',
    padding: 8,
    margin: 15,
    width: width/1.3,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 30,
    color: 'black',
    textAlign: 'center',
    marginBottom: 20,
  },

});
